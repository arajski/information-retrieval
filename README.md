# Information Retrieval

A simple example of search engine, using TF-IDF method to return relevant documents.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

### Prerequisites

Node.js: v8.4.0
NPM: v5.3.0

### Installing

To run this project, execute following commands on your environment:


```
git clone https://github.com/arajski/information-retrieval.git
cd information-retrieval
npm install
npm start
```

Application will be available under following url:

```
http://localhost:3000
```

## Built With

* [Express](https://expressjs.com/) - Web framework used for server development
* [porter-stemmer](https://www.npmjs.com/package/porter-stemmer) - Stemming algorithm
* [AngularJS](https://angularjs.org/) - Front End Framework


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
